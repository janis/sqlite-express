const express = require('express')
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('./db.sqlite');

const app = express()

app.use(express.json())

app.post('/:method', function (req, res) {
  console.log("---")
  console.log("Method:", req.params.method)
  console.log(req.body)

  db[req.params.method](req.body.query, req.body.params, (error, result) => {
    res.send( { error, result });
  });
})

app.listen(3000)